export class Request {
  username: string;
  brand: string;
  subBrand: string;
  model: string;
  licencePlate: string;
  serial: string;
  id: string;
}
