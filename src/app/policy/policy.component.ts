import { Component, OnInit } from '@angular/core';
import { Policy } from './policy';
import { PolicyService } from './policy.service';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html'
})
export class PolicyComponent implements OnInit {

  policies: Policy[];

  constructor(private policyService: PolicyService ) { }

  ngOnInit() {

    this.policyService.getPolicies().subscribe(
      policies => this.policies = policies
    );
  }

}
