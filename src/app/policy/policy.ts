import { Car } from './car';

export class Policy {
  id: string;
  username: string;
  car: Car;
  initialDate: string;
  urlpPath: string;
}
