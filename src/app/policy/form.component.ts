import { Component, OnInit } from '@angular/core';
import { Policy } from './policy';
import { PolicyService } from './policy.service';
import {Router, ActivatedRoute} from '@angular/router';
import { Request } from './request';
import { Car } from './car';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private policy: Policy = new Policy();
  private car: Car = new Car();
  private request: Request = new Request();
  private titulo: string = "Contratar póliza";


  constructor(private policyService: PolicyService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadPolicy();
  }

  public create(): void {
    this.policy.username = this.request.username;

    this.car.brand = this.request.brand;
    this.car.subBrand = this.request.subBrand;
    this.car.model = this.request.model;
    this.car.licencePlate = this.request.licencePlate;
    this.car.serial = this.request.serial;
    this.policy.car = this.car;

    console.log(this.policy);

    this.policyService.create(this.policy).subscribe(
      response => {this.router.navigate(['/policies']);
      swal('Póliza contratada', 'Se ha registrado la póliza', 'success');
    }
    );
  }

  public loadPolicy(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']

      if(id){
        this.policyService.getPolicy(id).subscribe( (policy) => { this.policy = policy;
        this.request.brand = this.policy.car.brand;
        this.request.subBrand = this.policy.car.subBrand;
        this.request.model = this.policy.car.model;
        this.request.licencePlate = this.policy.car.licencePlate;
        this.request.serial = this.policy.car.serial;
        this.request.username = this.policy.username;

        this.request.id = this.policy.id;
       });
      }
    })
  }

  public update():void{
    this.car.brand = this.request.brand;
    this.car.subBrand = this.request.subBrand;
    this.car.model = this.request.model;
    this.car.licencePlate = this.request.licencePlate;
    this.car.serial = this.request.serial;
    this.policy.car = this.car;

    console.log("!! " + this.request.id);

    this.policyService.update(this.policy,this.request.id)
    .subscribe( cliente => {
      this.router.navigate(['/policies'])
      swal('Póliza actualizada', `Póliza actualizada!`, 'success')
    }

    )
  }

}
