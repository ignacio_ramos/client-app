export class Car {
  brand: string;
  subBrand: string;
  model: string;
  licencePlate: string;
  serial;
}
