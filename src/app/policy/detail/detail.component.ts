import { Component, OnInit } from '@angular/core';
import {Policy} from '../policy';
import {PolicyService} from '../policy.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  policy: Policy;
  title:string = "Detalle de póliza";
  private selectedPhoto: File;

  constructor(private policyService: PolicyService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id: string = params.get('id');
      console.log("----");
      console.log("identi " + id);
      if(id) {
        this.policyService.getPolicy(id).subscribe(policy=>{
          this.policy = policy;
          console.log(this.policy);
        })
      }
    });
  }

  selectPhoto(event) {
    this.selectedPhoto = event.target.files[0];
    console.log("BJ "+ this.selectedPhoto);
  }

  uploadPhoto() {
    this.policyService.updloadImage(this.selectedPhoto, this.policy.id)
      .subscribe(policy => {
        this.policy = policy;
        swal('imagen creada correctamente','imagen creada correctamente','success');
      })

  }

}
