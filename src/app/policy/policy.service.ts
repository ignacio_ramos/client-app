import { Injectable } from '@angular/core';
import { Observable,throwError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Policy } from './policy';
import { Router } from '@angular/router';

import { AuthService } from '../cliente/auth.service';
import swal from 'sweetalert2';

@Injectable()
export class PolicyService {
  private endpoint1: string = 'http://localhost:3002/ramosvji-policy/ramosvji/api/v01/policy';
  private endpoint2: string = 'http://localhost:3002/ramosvji-policy/ramosvji/api/v01/policies';
  private endpoint3: string = 'localhost:8080/ramosvji/uploadFile';

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient, private router: Router,
  private authService: AuthService) {
    console.log("WW W " + this.addAuthorizationHeader())
  }

  getPolicies(): Observable<Policy[]> {
    return this.http.get<Policy[]>(this.endpoint2.concat('/ramosvji'),
    { headers: this.addAuthorizationHeader()})
    .pipe(
      catchError(e => {
        this.isNoAutorizado(e);
        return throwError(e);
      })
    //  map(response => response as Policy[])
    );
  }

  create(policy: Policy): Observable<Policy> {
    return this.http.post<Policy>(this.endpoint1,policy,{headers:this.addAuthorizationHeader()}).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
            return throwError(e);
        }
        return throwError(e);
      })
    );
  }

  getPolicy(id): Observable<Policy>{
    return this.http.get<Policy>(`${this.endpoint1}/${id}`,{ headers: this.addAuthorizationHeader()}).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        return throwError(e);
      })
    );
  }

  update(policy: Policy, id): Observable<Policy>{
    return this.http.patch<Policy>(`${this.endpoint1}/${id}/${policy.username}`,
      policy,
      {headers: this.addAuthorizationHeader()}).pipe(
          catchError(e => {
            if (this.isNoAutorizado(e)) {
              return throwError(e);
            }
            return throwError(e);
          })
      );
  }

  private isNoAutorizado(e): boolean {
    if(e.status == 401) {

      if(this.authService.isAuthenticated()) {
        this.authService.logout();
      }

      this.router.navigate(['login']);
      return true;
    }

    if(e.status == 403) {
      swal('Acceso denegado', 'No hay permisos para accerder','warning');
      this.router.navigate(['/policies']);
      return true;
    }

    return false;
  }

  private addAuthorizationHeader() {
   let token = this.authService.token;

   if (token != null) {
     return this.httpHeaders.append('Authorization', 'Bearer ' + token);
   }

   return this.httpHeaders;
 }

 updloadImage(image: File, id): Observable<Policy> {

    let formData = new FormData();
    formData.append("image", image);
    formData.append("id", id);

    /*const req = new HttpRequest('POST', this.endpoint3, formData, {
      reportProgress: true
    });*/

    //return this.http.request(req);
    return this.http.post(this.endpoint3,formData).pipe(
      map((response: any) => response.policy as Policy),
      catchError(e => {
        console.error(e.error.message);
        return throwError(e);
      })
    );

  }
}
