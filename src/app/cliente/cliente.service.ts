import { Injectable } from '@angular/core';
import { Client } from './client';
import { Observable, throwError } from 'rxjs';
import { HttpClient,  HttpHeaders, HttpRequest, HttpEvent  } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../cliente/auth.service';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable()
export class ClienteService {
  private endpoint: string = 'http://localhost:3002/ramosvji-clients/ramosvji/v01/clients';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient,
  private router: Router,private authService: AuthService ) {
  console.log("ddds " + this.addAuthorizationHeader()) }

  getCliente(): Observable<Client[]> {
    return this.http.get<Client[]> (this.endpoint,{ headers: this.addAuthorizationHeader()})
    .pipe(
      catchError(e => {
        this.isNoAutorizado(e);
        return throwError(e);
      })
      //map(response => response as Client[])
    );
  }

  private isNoAutorizado(e): boolean {
    if(e.status == 401 || e.status == 403) {
      this.router.navigate(['login']);
      return true;
    }
    return false;
  }

  private addAuthorizationHeader() {
   let token = this.authService.token;
   if (token != null) {
     return this.httpHeaders.append('Authorization', 'Bearer ' + token);
   }
   return this.httpHeaders;
 }
}
