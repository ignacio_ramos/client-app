import { Component, OnInit } from '@angular/core';
import { Client } from './client';
import { CLIENTES } from './clientes.json';
import { ClienteService } from './cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'
})
export class ClienteComponent implements OnInit {

  clients: Client[];

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.clienteService.getCliente().subscribe(
      clients => this.clients = clients
    );
  }

}
