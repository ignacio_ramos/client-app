import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  {id:1, nombre: 'ignacio', apellido: 'Ramos', email: 'ramosvji@gmail.com', createAt: '2017-12-27'},
  {id:2, nombre: 'ian', apellido: 'Ramos', email: 'ramosvji@gmail.com', createAt: '2017-12-27'},
  {id:3, nombre: 'jose', apellido: 'Ramos', email: 'ramosvji@gmail.com', createAt: '2017-12-27'}
];
