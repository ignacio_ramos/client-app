import { Component, OnInit } from '@angular/core';
import { Client } from './client';
import swal from 'sweetalert2';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  titulo: string = 'Por favor Sign In!';
  client: Client;

  constructor(private authService: AuthService, private router: Router) {
    this.client = new Client();
  }

  ngOnInit() {
    console.log("entrar")
    if (this.authService.isAuthenticated()) {
      swal('Login', 'Ya se inicio sesión!', 'info');
      this.router.navigate(['/policies']);
    }
  }

  login(): void {
    if(this.client.username== null || this.client.password == null) {
      swal('Error login','usuario o contraseña erroneas','error');
      return;
    }

    this.authService.login(this.client).subscribe(response => {
      this.authService.saveUsername(response.access_token);
      this.authService.saveToken(response.access_token);

      this.router.navigate(['/policies']);
      let client = this.authService.client;
      console.log("sesion " + client.username);
      swal('Login', 'Se ha iniciado sesión', 'success');
    }, error => {
      if (error.status == 400) {
        swal('Error Login', 'Credenciales incorrectas!', 'error');
      }
    } );
  }

}
