export class Client {
  name: string;
  lastname: string;
  username: string;
  email: string;
  password: string;
  roles: string[] = [];
}
