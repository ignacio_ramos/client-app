import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from './client';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _client: Client;
  private _token: string;

  constructor(private http: HttpClient) { }

  login(client: Client): Observable<any> {
    const endpoint: string = "http://localhost:3002/api/security/oauth/token";
    const credentials: string = btoa('polymerapp' + ':' + 'efAave7z');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credentials
    });

    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', client.username);
    params.set('password', client.password);

    return this.http.post<any>(endpoint, params.toString(), { headers: httpHeaders });
  }

  saveUsername(accessToken: string): void {
    let payload = this.getTokenData(accessToken);
    this._client = new Client();
    this._client.name = payload.name;
    this._client.lastname = payload.lastname;
    this._client.email = payload.email;
    this._client.username = payload.user_name;
    this._client.roles = payload.authorities;

    sessionStorage.setItem('client',JSON.stringify(this._client));
  }

  saveToken(accessToken: string): void {
    sessionStorage.setItem('jwtToken',accessToken);
  }

  getTokenData(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split(".")[1]));
    }
    return null;
  }

  public get client(): Client {
    if (this._client != null) {
      return this._client;
    } else if (this._client == null && sessionStorage.getItem('client') != null) {
      this._client = JSON.parse(sessionStorage.getItem('client')) as Client;
      return this._client;
    }
    return new Client();
  }

  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && sessionStorage.getItem('jwtToken') != null) {
      this._token = sessionStorage.getItem('jwtToken');
      return this._token;
    }
    return null;
  }

  isAuthenticated(): boolean {
    let payload = this.getTokenData(this.token);
    
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  logout(): void {
    this._token = null;
    this._client = null;
    sessionStorage.clear();
  }
}
