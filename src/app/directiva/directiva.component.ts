import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html'
})
export class DirectivaComponent {

  cursos: string[] = ['Java 8','Solidity','Swift','Scala','Javascript'];
  habilitar: boolean = true;

  setHabilitar(): void {
    this.habilitar = (this.habilitar==true)? false: true;
  }

  constructor() { }

}
