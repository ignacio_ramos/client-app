import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteService } from './cliente/cliente.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { PolicyComponent } from './policy/policy.component';
import { PolicyService } from './policy/policy.service';
import { FormComponent } from './policy/form.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './cliente/login.component';
import { DetailComponent } from './policy/detail/detail.component'

const routes: Routes = [
  {path: '', redirectTo: '/clientes', pathMatch: 'full'},
  {path: 'directivas', component: DirectivaComponent},
  {path: 'clientes', component: ClienteComponent},
  {path: 'policies', component: PolicyComponent},
  {path: 'policy/form', component: FormComponent},
  {path: 'policy/form/:id', component: FormComponent},
  {path: 'login', component: LoginComponent},
  {path: 'policies/policy/showdetail/:id', component: DetailComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClienteComponent,
    PolicyComponent,
    FormComponent,
    LoginComponent,
    DetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [ClienteService,
    PolicyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
